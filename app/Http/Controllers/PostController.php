<?php

namespace App\Http\Controllers;

use App\Post;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class PostController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function __construct(){
        $this->middleware('auth');
    }

    public function index()
    {
//        $posts = DB::table('posts');
        $posts = Post::OrderBy('created_at','desc')->paginate(6);
        return view('index',['posts' => $posts]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('create');
    }

    public function search(Request $request)
    {
        $search = $request->get('search');
        $posts  = Post::Where('title','like','%'.$search.'%')
            ->orWhere('author','like','%'.$search.'%')
            ->paginate(6);
        return view('index',['posts'=> $posts]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $new_name = rand().'.'.$request->file->extension();
        $request->file->move(public_path('image'), $new_name);

        $posts = new Post();
        $posts->title = $request->input('title');
        $posts->detail = $request->input('detail');
        $posts->author = $request->input('author');
        $posts->new_name = $new_name;
        $posts->save();

        if($posts){
            $red = redirect('posts')->with('toast_success','Data already add');
        } else{
            $red = redirect('posts/create')->with('danger','Input fail');
        }
        return $red;

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $posts = DB::select('select * from posts where id=?',[$id]);
        return view('show',['posts'=> $posts]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $posts = DB::select('select * from posts where id=?',[$id]);
        return view('edit',['posts' => $posts]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {

        $posts = Post::find($id);
        $posts->title = $request->input('title');
        $posts->detail = $request->input('detail');
        $posts->author = $request->input('author');
        $posts->save();
        if($posts){
            $red = redirect('posts')->with('toast_success','Data already update');
        }else{
            $red = redirect('posts/edit', $id)->with('danger','Error update');
        }
        return $red;
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $posts = DB::delete('delete from posts where id=?',[$id]);
        $red = redirect('posts');
        if($posts){
            $red = redirect('posts')->with('toast_success','Data already delete');
        }else{
            $red = redirect('posts/edit', $id)->with('danger','Error update');
        }
        return $red;
    }
}
