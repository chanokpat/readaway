<?php


Route::get('/','PostController@index');
Route::get('/search','PostController@search');
Route::resource('posts','PostController');

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
