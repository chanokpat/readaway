@extends('layouts')
@section('content')

    @if($message =Session::get("success"))
    @endif
    <div class="row">
        <div class="col-md-6">
            <h1>Readaway</h1>
        </div>
        <div class="col-md-4">
            <form action="/search" method="get">
                <div class="input-group">
                    <input type="search" name="search" class="form-control">
                     <span class="input-group-prepend">
                         <button type="submit" class="btn btn-outline-primary">Search</button>
                     </span>
                </div>
            </form>
        </div>
        <div class="col-2 text-right">
            <a href="{{ action('PostController@create') }}" class="btn btn-success">Create</a>
        </div>
    </div>

    <div class="container" style="font-family: 'Kanit';">
        <div class="row">
            @foreach($posts as $post)
                <div class="col-md-4">
                    <div class="card border-dark mb-3">
                        <img class="card-img-top img-fluid img-thumbnail" src="{{ url('image/'.$post->new_name) }}" max-width="120" alt="Card image cap">
                        <h5><div class="card-header bg-transparent text-darks">{{ $post->title }}</div></h5>
                        <div class="card-body text-secondary">{{ $post->author }}</div>
                        <div class="card-footer bg-transparent border-success">
                            <form action="{{ action('PostController@destroy', $post->id) }}" method="post">
                                <a href="{{ action('PostController@show', $post->id) }}" class="btn btn-primary btn-sm">Show</a>
                                <a href="{{ action('PostController@edit', $post->id) }}" class="btn btn-outline-warning btn-sm">Edit</a>
                                @csrf
                                @method('DELETE')
                                <button type="submit" class="btn btn-outline-danger btn-sm">Delete</button>
                            </form>
                        </div>
                    </div>
                </div>
            @endforeach
        </div>
    </div>

    {{ $posts->links() }}
@endsection





