@extends('layouts')
@section('content')

    <div class="container" style="font-family: 'Kanit';">
        <a href="{{ action('PostController@index') }}" class="btn btn-primary mb-4">Back</a>
        <div class="row">
            @foreach($posts as $post)
            <div class="col-md-4">
                <img class="card-img-top img-fluid img-thumbnai" src="{{ url('image/'.$post->new_name) }}" width="120">
            </div>
            <div class="col-md-6">
                <h3><div class="card-title">{{ $post->title }}</div></h3>
                <p class="card-text">{{ $post->author }}</p>
                <p class="card-text">{{ $post->detail }}</p>
            </div>
            @endforeach
        </div>
    </div>

@endsection
