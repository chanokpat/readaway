@extends('layouts')
@section('content')

    <div class="row">
        <div class="col-md-6">
            @foreach($posts as $posts)
                <form action="{{ action('PostController@update', $posts->id) }}" method="post">
                    @csrf
                    @method('PUT')
                    <div class="form-group">
                        <label>Image</label>
                        <input class="form-control" type="file" name="file"/>
                    </div>
                    <div class="form-group">
                        <label>Title</label>
                        <input type="text" name="title" class="form-control" value="{{ $posts->title }}">
                    </div>
                    <div class="form-group">
                        <label>Detail</label>
                        <textarea name="detail" class="form-control">{{ $posts->detail }}</textarea>
                    </div>
                    <div class="form-group">
                        <label>Author</label>
                        <input type="text" name="author" class="form-control" value="{{ $posts->author }}">
                    </div>
                    <button type="submit" class="btn btn-warning">Update</button>
                    <a href="{{ action('PostController@index') }}" class="btn btn-default">Back</a>
                </form>
             @endforeach
        </div>
    </div>
@endsection
