@extends('layouts')
@section('content')

<div class="row">
    <div class="col-md-6 offset-md-3">
        @if($errors->any())
            <div class="alert alert-danger" >
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                @foreach($errors->all() as $error)
                    {{ $error }} <br>
                 @endforeach
            </div>
         @endif


        <form action="{{ action('PostController@store') }}" method="post" enctype="multipart/form-data">
            @csrf
            <div class="form-group">
                <label>Image</label>
                <input class="form-control" type="file" name="file"/>
            </div>
            <div class="form-group">
                <label>Title</label>
                <input class="form-control" type="text" name="title" placeholder="Title"/>
            </div>
            <div class="form-group">
                <label>Detail</label>
                <textarea class="form-control" name="detail" placeholder="Detail"></textarea>
            </div>
            <div class="form-group">
                <label>Author</label>
                <input class="form-control" type="text" name="author" placeholder="Author"/>
            </div>
            <button class="btn btn btn-primary">submit</button>
        </form>
    </div>
</div>

@include('sweetalert::alert')
@endsection
