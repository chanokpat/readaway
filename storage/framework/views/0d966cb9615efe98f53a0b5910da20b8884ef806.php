<?php $__env->startSection('content'); ?>

<div class="row">
    <div class="col-md-6 offset-md-3">
        <?php if($errors->any()): ?>
            <div class="alert alert-danger" >
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                <?php $__currentLoopData = $errors->all(); $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $error): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                    <?php echo e($error); ?> <br>
                 <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
            </div>
         <?php endif; ?>


        <form action="<?php echo e(action('PostController@store')); ?>" method="post" enctype="multipart/form-data">
            <?php echo csrf_field(); ?>
            <div class="form-group">
                <label>Image</label>
                <input class="form-control" type="file" name="file"/>
            </div>
            <div class="form-group">
                <label>Title</label>
                <input class="form-control" type="text" name="title" placeholder="Title"/>
            </div>
            <div class="form-group">
                <label>Detail</label>
                <textarea class="form-control" name="detail" placeholder="Detail"></textarea>
            </div>
            <div class="form-group">
                <label>Author</label>
                <input class="form-control" type="text" name="author" placeholder="Author"/>
            </div>
            <button class="btn btn btn-primary">submit</button>
        </form>
    </div>
</div>

<?php echo $__env->make('sweetalert::alert', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('layouts', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH C:\xampp\htdocs\readaway\resources\views/create.blade.php ENDPATH**/ ?>