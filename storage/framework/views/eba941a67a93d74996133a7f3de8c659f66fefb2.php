<?php $__env->startSection('content'); ?>

    <?php if($message =Session::get("success")): ?>
    <?php endif; ?>
    <div class="row">
        <div class="col-md-6">
            <h1>Readaway</h1>
        </div>
        <div class="col-md-4">
            <form action="/search" method="get">
                <div class="input-group">
                    <input type="search" name="search" class="form-control">
                     <span class="input-group-prepend">
                         <button type="submit" class="btn btn-outline-primary">Search</button>
                     </span>
                </div>
            </form>
        </div>
        <div class="col-2 text-right">
            <a href="<?php echo e(action('PostController@create')); ?>" class="btn btn-success">Create</a>
        </div>
    </div>

    <div class="container" style="font-family: 'Kanit';">
        <div class="row">
            <?php $__currentLoopData = $posts; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $post): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                <div class="col-md-4">
                    <div class="card border-dark mb-3">
                        <img class="card-img-top img-fluid img-thumbnail" src="<?php echo e(url('image/'.$post->new_name)); ?>" max-width="120" alt="Card image cap">
                        <h5><div class="card-header bg-transparent text-darks"><?php echo e($post->title); ?></div></h5>
                        <div class="card-body text-secondary"><?php echo e($post->author); ?></div>
                        <div class="card-footer bg-transparent border-success">
                            <form action="<?php echo e(action('PostController@destroy', $post->id)); ?>" method="post">
                                <a href="<?php echo e(action('PostController@show', $post->id)); ?>" class="btn btn-primary btn-sm">Show</a>
                                <a href="<?php echo e(action('PostController@edit', $post->id)); ?>" class="btn btn-outline-warning btn-sm">Edit</a>
                                <?php echo csrf_field(); ?>
                                <?php echo method_field('DELETE'); ?>
                                <button type="submit" class="btn btn-outline-danger btn-sm">Delete</button>
                            </form>
                        </div>
                    </div>
                </div>
            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
        </div>
    </div>

    <?php echo e($posts->links()); ?>

<?php $__env->stopSection(); ?>






<?php echo $__env->make('layouts', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH C:\xampp\htdocs\readaway\resources\views/index.blade.php ENDPATH**/ ?>