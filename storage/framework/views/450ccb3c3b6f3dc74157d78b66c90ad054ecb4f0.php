<?php $__env->startSection('content'); ?>

    <div class="row">
        <div class="col-md-6">
            <?php $__currentLoopData = $posts; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $posts): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                <form action="<?php echo e(action('PostController@update', $posts->id)); ?>" method="post">
                    <?php echo csrf_field(); ?>
                    <?php echo method_field('PUT'); ?>
                    <div class="form-group">
                        <label>Image</label>
                        <input class="form-control" type="file" name="file"/>
                    </div>
                    <div class="form-group">
                        <label>Title</label>
                        <input type="text" name="title" class="form-control" value="<?php echo e($posts->title); ?>">
                    </div>
                    <div class="form-group">
                        <label>Detail</label>
                        <textarea name="detail" class="form-control"><?php echo e($posts->detail); ?></textarea>
                    </div>
                    <div class="form-group">
                        <label>Author</label>
                        <input type="text" name="author" class="form-control" value="<?php echo e($posts->author); ?>">
                    </div>
                    <button type="submit" class="btn btn-warning">Update</button>
                    <a href="<?php echo e(action('PostController@index')); ?>" class="btn btn-default">Back</a>
                </form>
             <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
        </div>
    </div>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('layouts', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH C:\xampp\htdocs\readaway\resources\views/edit.blade.php ENDPATH**/ ?>