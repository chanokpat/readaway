<?php $__env->startSection('content'); ?>

    <div class="container" style="font-family: 'Kanit';">
        <a href="<?php echo e(action('PostController@index')); ?>" class="btn btn-primary mb-4">Back</a>
        <div class="row">
            <?php $__currentLoopData = $posts; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $post): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
            <div class="col-md-4">
                <img class="card-img-top img-fluid img-thumbnai" src="<?php echo e(url('image/'.$post->new_name)); ?>" width="120">
            </div>
            <div class="col-md-6">
                <h3><div class="card-title"><?php echo e($post->title); ?></div></h3>
                <p class="card-text"><?php echo e($post->author); ?></p>
                <p class="card-text"><?php echo e($post->detail); ?></p>
            </div>
            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
        </div>
    </div>

<?php $__env->stopSection(); ?>

<?php echo $__env->make('layouts', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH C:\xampp\htdocs\readaway\resources\views/show.blade.php ENDPATH**/ ?>